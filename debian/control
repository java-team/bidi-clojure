Source: bidi-clojure
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders:
 Apollon Oikonomopoulos <apoikos@debian.org>,
 Jérôme Charaoui <jerome@riseup.net>,
Section: java
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 default-jdk-headless,
 javahelper,
 leiningen,
 libclojure-java,
 libcomplete-clojure <!nocheck>,
 libcompojure-clojure,
 libnrepl-clojure <!nocheck>,
 libprismatic-schema-clojure (>= 1.1.12),
 libring-core-clojure (>= 1.6.2-4),
 libring-mock-clojure (>= 0.4.0-2),
 libtools-reader-clojure (>= 1.3.4),
 maven-repo-helper,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/clojure-team/bidi-clojure
Vcs-Git: https://salsa.debian.org/clojure-team/bidi-clojure.git
Homepage: https://github.com/juxt/bidi
Rules-Requires-Root: no

Package: libbidi-clojure
Architecture: all
Depends:
 libclojure-java,
 libprismatic-schema-clojure (>= 1.1.12),
 libring-core-clojure (>= 1.6.2-4),
 ${java:Depends},
 ${misc:Depends},
Recommends:
 ${java:Recommends},
Description: bidirectional URI routing for Clojure
 Bi-directional URI dispatch. Like compojure, but when you want to go both
 ways. If you are serving REST resources, you should be
 providing links to other resources, and without full support for forming URIs
 from handlers your code will become coupled with your routing. In short,
 hard-coded URIs will eventually break.
 .
 bidi provides a way to construct URIs from handlers.
